import 'package:get/get.dart';
import 'package:projek_kel_2/model/peminjam.dart';

class CPeminjam extends GetxController {
  Rx<Peminjam> _peminjam = Peminjam().obs;

  Peminjam get user => _peminjam.value;

  void setUser(Peminjam dataPeminjam) => _peminjam.value = dataPeminjam;
}
