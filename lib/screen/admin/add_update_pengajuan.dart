import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:projek_kel_2/config/asset.dart';
import 'package:projek_kel_2/event/event_db.dart';
import 'package:projek_kel_2/screen/admin/list_pengajuan.dart';
import 'package:projek_kel_2/widget/info.dart';

import '../../model/pengajuan.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final Pengajuan? pengajuan;
  AddUpdatePengajuan({this.pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKode = TextEditingController();
  var _controllerTanggal = TextEditingController();
  var _controllerNpm = TextEditingController();
  var _controllerNama = TextEditingController();
  var _controllerProdi = TextEditingController();
  var _controllerNo_hp = TextEditingController();


  bool get_navigation = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengajuan != null) {
      _controllerKode.text = widget.pengajuan!.kode_pengajuan!;
      _controllerTanggal.text = widget.pengajuan!.tanggal!;
      _controllerNpm.text = widget.pengajuan!.npm_peminjam!;
      _controllerNama.text = widget.pengajuan!.nama_peminjam!;
      _controllerProdi.text = widget.pengajuan!.prodi!;
      _controllerNo_hp.text = widget.pengajuan!.no_hp!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengajuan != null
            ? Text('Update Pengajuan')
            : Text('Tambah Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKode,
                  decoration: InputDecoration(
                      labelText: "Kode",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggal,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNpm,
                  decoration: InputDecoration(
                      labelText: "Npm",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerProdi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNo_hp,
                  decoration: InputDecoration(
                      labelText: "No_hp",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengajuan == null) {
                        String message = await EventDb.addPengajuan(
                          _controllerKode.text,
                          _controllerTanggal.text,
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerProdi.text,
                          _controllerNo_hp.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKode.clear();
                          _controllerTanggal.clear();
                          _controllerNpm.clear();
                          _controllerNama.clear();
                          _controllerProdi.clear();
                          _controllerNo_hp.clear();
                          Get.off(
                            ListPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuan(
                          _controllerKode.text,
                          _controllerTanggal.text,
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerProdi.text,
                          _controllerNo_hp.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
