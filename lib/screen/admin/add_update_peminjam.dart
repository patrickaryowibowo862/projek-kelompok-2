import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:projek_kel_2/config/asset.dart';
import 'package:projek_kel_2/event/event_db.dart';
import 'package:projek_kel_2/screen/admin/list_peminjam.dart';
import 'package:projek_kel_2/widget/info.dart';

import '../../model/peminjam.dart';

class AddUpdatePeminjam extends StatefulWidget {
  final Peminjam? peminjam;
  AddUpdatePeminjam({this.peminjam});

  @override
  State<AddUpdatePeminjam> createState() => _AddUpdatePeminjamState();
}

class _AddUpdatePeminjamState extends State<AddUpdatePeminjam> {
  var _formKey = GlobalKey<FormState>();
  var _controllerNpm = TextEditingController();
  var _controllerNama = TextEditingController();
  var _controllerProdi = TextEditingController();
  var _controllerFakultas = TextEditingController();

  bool get_navigation = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.peminjam != null) {
      _controllerNpm.text = widget.peminjam!.npm!;
      _controllerNama.text = widget.peminjam!.nama!;
      _controllerProdi.text = widget.peminjam!.prodi!;
      _controllerFakultas.text = widget.peminjam!.fakultas!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.peminjam != null
            ? Text('Update Peminjam')
            : Text('Tambah Peminjam'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.peminjam == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNpm,
                  decoration: InputDecoration(
                      labelText: "NPM",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerProdi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerFakultas,
                  decoration: InputDecoration(
                      labelText: "Fakultas",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.peminjam == null) {
                        String message = await EventDb.addPeminjam(
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerProdi.text,
                          _controllerFakultas.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerNpm.clear();
                          _controllerNama.clear();
                          _controllerProdi.clear();
                          _controllerFakultas.clear();
                          Get.off(
                            ListPeminjam(),
                          );
                        }
                      } else {
                        EventDb.UpdatePeminjam(
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerFakultas.text,
                          _controllerProdi.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.peminjam == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
