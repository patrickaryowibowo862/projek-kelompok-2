import 'package:flutter/material.dart';

class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  void _onMenuClick(String menu) {
    Navigator.pop(context); // Tutup sidebar menu

    // Tampilkan pesan sesuai dengan menu yang diklik
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Menu yang diklik: $menu'),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          color: const Color.fromARGB(255, 255, 166, 0),
          child: Column(
            children: [
              // App Bar
              Container(
                height: 60,
                color: Color.fromARGB(143, 230, 236, 216),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        _scaffoldKey.currentState!.openDrawer();
                      },
                    ),
                    Text('Form Screen'),
                  ],
                ),
              ),
              // Konten
              Expanded(
                child: Center(
                  child: Text('Konten Form'),
                ),
              ),
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 255, 166, 0),
              ),
              accountName: Text('Erick'),
              accountEmail: Text('patrick_aryo_wibowo@teknokrat.ac.id'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('asset/image/teknokrat.png'), // Ganti dengan path gambar Anda
              ),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Menu 1'),
              onTap: () {
                _onMenuClick('Menu 1');
              },
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Menu 2'),
              onTap: () {
                _onMenuClick('Menu 2');
              },
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Menu 3'),
              onTap: () {
                _onMenuClick('Menu 3');
              },
            ),
          ],
        ),
      ),
    );
  }
}
