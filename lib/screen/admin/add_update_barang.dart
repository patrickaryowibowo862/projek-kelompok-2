import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:projek_kel_2/config/asset.dart';
import 'package:projek_kel_2/event/event_db.dart';
import 'package:projek_kel_2/screen/admin/list_barang.dart';
import 'package:projek_kel_2/widget/info.dart';

import '../../model/barang.dart';

class AddUpdateBarang extends StatefulWidget {
  final Barang? barang;
  AddUpdateBarang({this.barang});

  @override
  State<AddUpdateBarang> createState() => _AddUpdateBarangState();
}

class _AddUpdateBarangState extends State<AddUpdateBarang> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKode = TextEditingController();
  var _controllerNama = TextEditingController();
  var _controllerJumlah = TextEditingController();

  bool get_navigation = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.barang != null) {
      _controllerKode.text = widget.barang!.kode_barang!;
      _controllerNama.text = widget.barang!.nama_barang!;
      _controllerJumlah.text = widget.barang!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.barang != null
            ? Text('Update Barang')
            : Text('Tambah Barang'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.barang == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKode,
                  decoration: InputDecoration(
                      labelText: "Kode",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerJumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.barang == null) {
                        String message = await EventDb.addBarang(
                          _controllerKode.text,
                          _controllerNama.text,
                          _controllerJumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKode.clear();
                          _controllerNama.clear();
                          _controllerJumlah.clear();
                          Get.off(
                            ListBarang(),
                          );
                        }
                      } else {
                        EventDb.UpdateBarang(
                          _controllerKode.text,
                          _controllerNama.text,
                          _controllerJumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.barang == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
