import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:projek_kel_2/config/asset.dart';
import 'package:projek_kel_2/event/event_db.dart';
import 'package:projek_kel_2/screen/admin/add_update_peminjam.dart';

import '../../model/peminjam.dart';


class ListPeminjam extends StatefulWidget {
  @override
  State<ListPeminjam> createState() => _ListPeminjamState();
}

class _ListPeminjamState extends State<ListPeminjam> {
  List<Peminjam> _listPeminjam = [];

  void getPeminjam() async {
    _listPeminjam = await EventDb.getPeminjam();

    setState(() {});
  }

  @override
  void initState() {
    getPeminjam();
    super.initState();
  }

  void showOption(Peminjam? peminjam) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePeminjam(peminjam: peminjam))
            ?.then((value) => getPeminjam());
        break;
      case 'delete':
        EventDb.deletePeminjam(peminjam!.npm!)
            .then((value) => getPeminjam());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Peminjam'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPeminjam.length > 0
              ? ListView.builder(
                  itemCount: _listPeminjam.length,
                  itemBuilder: (context, index) {
                    Peminjam peminjam = _listPeminjam[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(peminjam.nama ?? ''),
                      subtitle: Text(peminjam.npm ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(peminjam),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePeminjam())?.then((value) => getPeminjam()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
