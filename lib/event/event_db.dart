import 'dart:convert';
import 'package:get/get.dart';
import 'package:projek_kel_2/config/api.dart';
import 'package:projek_kel_2/event/event_pref.dart';
import 'package:projek_kel_2/model/barang.dart';
import 'package:projek_kel_2/model/pengajuan.dart';
import 'package:projek_kel_2/model/pengembalian.dart';
import 'package:projek_kel_2/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:projek_kel_2/screen/login.dart';
import 'package:projek_kel_2/widget/info.dart';

import '../model/peminjam.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              Login(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  // =============Peminjam============
  static Future<List<Peminjam>> getPeminjam() async {
    List<Peminjam> listPeminjam = [];

    try {
      var response = await http.get(Uri.parse(Api.getPeminjam));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var peminjam = responBody['peminjam'];

          peminjam.forEach((peminjam) {
            listPeminjam.add(Peminjam.fromJson(peminjam));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPeminjam;
  }

    static Future<String> addPeminjam(String npm, String nama,
       String prodi, String fakultas) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPeminjam), body: {
        'npm': npm,
        'nama': nama,
        'prodi': prodi,
        'fakultas': fakultas,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Peminjam Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }
    static Future<void> UpdatePeminjam(String npm, String nama,
       String Prodi, String fakultas) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePeminjam), body: {
        'npm': npm,
        'nama': nama,
        'prodi': Prodi,
        'fakultas': fakultas
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Peminjam');
        } else {
          Info.snackbar('Gagal Update Peminjam');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  static Future<void> deletePeminjam(String npm) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePeminjam), body: {'npm': npm});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Peminjam');
        } else {
          Info.snackbar('Gagal Delete Peminjam');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //==============Barang=================
  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['barang'];

          barang.forEach((barang) {
            listBarang.add(Barang.fromJson(barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }

    static Future<String> addBarang(String kode_barang, String nama_barang,
       String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }
    static Future<void> UpdateBarang(String kode_barang, String nama_barang,
       String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update barang');
        } else {
          Info.snackbar('Gagal Update barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  static Future<void> deleteBarang(String kode_barang) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteBarang), body: {'kode_barang': kode_barang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Barang');
        } else {
          Info.snackbar('Gagal Delete Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

    //==============Pengajuan=================
static Future<List<Pengajuan>> getPengajuan() async {
    List<Pengajuan> listPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuan = responBody['pengajuan'];

          pengajuan.forEach((pengajuan) {
            listPengajuan.add(Pengajuan.fromJson(pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuan;
  }
      static Future<String> addPengajuan(String kode_pengajuan, String tanggal,
       String npm_peminjam, String nama_peminjam, String prodi, String no_hp) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal' : tanggal,
        'npm_peminjam' : npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'no_hp' : no_hp,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }
    static Future<void> UpdatePengajuan(String kode_pengajuan, String tanggal,
       String npm_peminjam, String nama_peminjam, String prodi, String no_hp) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal' : tanggal,
        'npm_peminjam' : npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'no_hp' : no_hp,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Pengajuan Berhasil');
        } else {
          Info.snackbar('Gagal Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  static Future<void> deletePengajuan(String kode_pengajuan)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengajuan), body: {'kode_pengajuan': kode_pengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete pengajuan');
        } else {
          Info.snackbar('Gagal Delete pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

      //==============Pengembalian=================
      static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['pengembalian'];

          pengembalian.forEach((pengajuan) {
            listPengembalian.add(Pengembalian.fromJson(pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }
      static Future<String> addPengembalian(String kode_pengembalian,String kode_pengajuan,String tanggal_kembali) async {
    String reason;
    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan' : kode_pengajuan,
        'tanggal_kembali' : tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }
    static Future<void> UpdatePengembalian(String kode_pengembalian,String kode_pengajuan,String tanggal_kembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan' : kode_pengajuan,
        'tanggal_kembali' : tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Pengembalian Berhasil');
        } else {
          Info.snackbar('Gagal Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  static Future<void> deletePengembalian(String kode_pengembalian)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengembalian), body: {'kode_pengembalian': kode_pengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete pengembalian');
        } else {
          Info.snackbar('Gagal Delete pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  
}
