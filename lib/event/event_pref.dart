import 'dart:convert';
import 'package:projek_kel_2/model/peminjam.dart';
import 'package:projek_kel_2/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EventPref {
  // untuk simpan data di shared preferences
  static void saveUser(User user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('user', jsonEncode(user));
  }
    static void savePeminjam(Peminjam peminjam) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('peminjam', jsonEncode(peminjam));
  }


  //get data user
  static Future<User?> getUser() async {
    User? user;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? stringUser = pref.getString('user');

    if (stringUser != null) {
      Map<String, dynamic> mapUser = jsonDecode(stringUser);
      user = User.fromJson(mapUser);
    }
    return user;
  }

    //get data peminjam
  static Future<Peminjam?> getPeminjam() async {
    Peminjam? peminjam;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? stringPeminjam = pref.getString('peminjam');

    if (stringPeminjam != null) {
      Map<String, dynamic> mapUser = jsonDecode(stringPeminjam);
      peminjam = Peminjam.fromJson(mapUser);
    }
    return peminjam;
  }

  //hapus data user di shared preferences
  static void clear() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }
}
