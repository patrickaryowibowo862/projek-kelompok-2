class Pengajuan {
  String? kode_pengajuan;
  String? tanggal;
  String? npm_peminjam;
  String? nama_peminjam;
  String? prodi;
  String? no_hp;

  Pengajuan({
    this.kode_pengajuan,
    this.tanggal,
    this.npm_peminjam,
    this.nama_peminjam,
    this.prodi,
    this.no_hp,
  });

  factory Pengajuan.fromJson(Map<String, dynamic> json) => Pengajuan(
        kode_pengajuan: json['kode_pengajuan'],
        tanggal: json['tanggal'],
        npm_peminjam: json['npm_peminjam'],
        nama_peminjam: json['nama_peminjam'],
        prodi: json['prodi'],
        no_hp: json['no_hp'],
      );

  Map<String, dynamic> toJson() => {
      'kode_pengajuan': this.kode_pengajuan,
      'tanggal': this.tanggal,
      'npm_peminjam':this.npm_peminjam,
      'nama_peminjam':this.nama_peminjam,
      'prodi':this.prodi,
      'no_hp':this.no_hp,
      };
}
