class Peminjam {
  String? npm;
  String? nama;
  String? prodi;
  String? fakultas;

  Peminjam({
    this.npm,
    this.nama,
    this.prodi,
    this.fakultas,
  });

  factory Peminjam.fromJson(Map<String, dynamic> json) => Peminjam(
        npm: json['npm'],
        nama: json['nama'],
        prodi: json['prodi'],
        fakultas: json['fakultas'],
      );

  Map<String, dynamic> toJson() => {
        'npm': this.npm,
        'nama': this.nama,
        'prodi': this.prodi,
        'fakultas': this.fakultas,
      };
}
